## DownUnder CTF 2020

* **CTF Time page:** https://ctftime.org/event/1084
* **Category:** Jeopardy
* **Date:** Fri, 18 Sept. 2020, 19:00 AEST — Sun, 20 Sept. 2020, 19:00 AEST

---

### Solved Challenges
| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
| addition | misc | 200 | python |
| formatting | rev | 100 | static analysis, live analysis |
| koala habitat | misc | 499 | audio, waveform, morse code | 
| shell this! | pwn | 100 | bof |
| pretty good pitfall | misc | 200 | gpg, pgp | 
