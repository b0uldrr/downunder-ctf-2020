## Pretty Good Pitfall

* **Category:**  Misc
* **Points:** 200

### Challenge
```
Author: k0wa1ski#6150

PGP/GPG/GnuPG/OpenPGP is great! I reckon you can't find the message, because it looks scrambled!
```

### Downloads
* [flag.txt.gpg](flag.txt.gpg)

### Solution
* **Author(s):** b0uldrr
* **Date:** 18/09/20

I downloaded the provided file, `flag.txt.gpg`. Opening it in a text editor, the contents appears to be binary data. Running `file` on it just tells us that the file is data. I tried using `gpg` with the `-d` option to decrypt it... Turns out the file isn't encrypted, just signed. This displays the flag.

```
$ gpg -d flag.txt.gpg 
DUCTF{S1GN1NG_A1NT_3NCRYPT10N}
gpg: Signature made Mon 07 Sep 2020 18:46:12 AEST
gpg:                using RSA key 3A83778AE59F8A5068930CE191F31AFE193132C8
gpg: Can't check signature: No public key
```

### Flag 
```
DUCTF{S1GN1NG_A1NT_3NCRYPT10N}
```
