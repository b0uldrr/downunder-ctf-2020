## Koala Habitat

* **Category:** Misc
* **Points:** 499

### Challenge
```
Author: QUT_WH

What an Aussie Banger!

Flag Format: STRING you end up with after solving challenge --> Spaces seperate the words

NO DUCTF{} required
```

### Downloads
* [gumtrees.wav](gumtrees.wav)

### Solution
* **Author(s):** b0uldrr
* **Date:** 18/09/20

I downloaded the provided wav file and played it. It was a song with some incoherent beeping over the top. I opened the file in audacity to examine the waveform and could make out some long and short pulses which appeared be be morse code. I decoded the message manually which provided the below flag.

![waveform](images/waveform.png)

### Flag 
```
CARAMELLO KOALAS ARE THE BEST KOALAS
```
