## Formatting 

* **Category:** Reversing 
* **Points:** 100 

### Challenge
```
Author: h4sh5

Its really easy, I promise
```

### Downloads
* [formatting](formatting)

### Solution
* **Author(s):**  b0uldrr
* **Date:**  18/09/20

I downloaded the provided binary and ran it. It just prints `haha its not that easy}` before quitting.

I opened the program in Ghidra. We can find a `main` function which defines a bunch of variables and then calls `sprintf` and later `puts`. The `puts` call with `flag` is red herring fake flag.

![main](images/main.png)
![fake](images/fake_flag.png) 

I wasn't really sure what was happening in this otherwise very simple function, so I opened it in `gdb` to see what was happening on the stack.

I ran the program within `gdb` to load it into memory, then ran `disas main` to disassemble the main function and an appropriate breakpoint. I set a breakpoint at the `sprintf` call at 0x0000555555555204 and then ran the program again. Executing it and stepping through to the next command, we can see the flag in the stack at 0x7fffffffe040: 

![flag](images/flag.png)

### Flag 
```
DUCTF{d1d_You_Just_ltrace_296faa2990acbc36}
```
