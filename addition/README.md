## Addition 

* **Category:** Misc 
* **Points:** 200

### Challenge
```
Joe is aiming to become the next supreme coder by trying to make his code smaller and smaller. His most recent project is a simple calculator which he reckons is super secure because of the "filters" he has in place. However, he thinks that he knows more than everyone around him. Put Joe in his place and grab the flag.

https://chal.duc.tf:30302/
```

### Solution
* **Author(s):** b0uldrr
* **Date:** 20/09/20 

The website is a calculator which seems to have the same functionality as an interactive python prompt, however some inputs are filtered out.

![1](images/1.png)

Issuing the command `globals()` prints all global variables, including the flag.

![2](images/2.png)

### Flag 
```
DUCTF{3v4L_1s_D4ng3r0u5}
```
